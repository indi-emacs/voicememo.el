;;; voicememo.el --- Transcribe Voicememos and create org-headings from them -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2024 Vincent Trötschel
;;
;; Author: Vincent Trötschel <indiana@Indianss-MBP.fritz.box>
;; Maintainer: Vincent Trötschel <indiana@Indianss-MBP.fritz.box>
;; Created: January 11, 2024
;; Modified: January 11, 2024
;; Version: 0.0.1
;; Keywords: abbrev bib c calendar comm convenience data docs emulations extensions faces files frames games hardware help hypermedia i18n internal languages lisp local maint mail matching mouse multimedia news outlines processes terminals tex tools unix vc wp
;; Homepage: https://github.com/indiana/voicememo
;; Package-Requires: ((emacs "29.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Transcribe Voicememos and create org-headings from them.
;;  TODO .lastseen is updated to refer to the audiomemo with the biggest timestamp
;;  (i.e. the latest audiomemo) of any invocation.
;;  However if one with a smaller timestamp fails it will never be retried.
;;  This is not really a problem, but maybe me need a "transcribe-memo-at-point" fn to fix that.
;;
;;; Code:
(require 'org-id)
(require 'org-element)
;;; TODO whisper.el isn't really necessary the few lines of code that are used
;;; should be placed in this file. But why not use the same config-api that way duplication
;;; can be prevented at emacs config time, i.e.:
;;; `(setq voicememo--whisper-cmd whisper-command)'
(require 'whisper)

(defgroup voicememo nil
  "Interface turning Voicerecordings into org entries with transcriptions."
  :group 'external)

(defcustom voicememo-target-file (expand-file-name "inbox.org" org-directory)
  "The file where the org entries and transcriptions are appended to."
  :type 'file
  :group 'voicememo)

(defcustom voicememo-src-dir (expand-file-name "voice-inbox" org-directory)
  "Directory that stores recordings to be transcribed.
Defaults to ~/org/voice-inbox."
  :type 'directory
  :group 'voicememo)

(defcustom voicememo-lastseen-file (expand-file-name ".last-seen" voicememo-src-dir)
  "The file tracking the timestamp of the last parsing operation.
Defaults to ~/org/voicememos/.last-seen."
  :type 'file
  :group 'voicememo)

(defcustom voicememo-status-prop "TRANSCRIPTION_STATUS"
  "Property key in org entries denoting the transcription status."
  :type 'string
  :group 'voicememo)

(defcustom voicememo-file-prop "AUDIOFILE"
  "Property key in org entries denoting the location of the audiofile."
  :type 'string
  :group 'voicememo)

(defcustom voicememo-record-time-prop "RECORDED"
  "Property key in org entries denoting the time the file was recorded."
  :type 'string
  :group 'voicememo)

(defcustom voicememo-max-whisper-threads '(("large-v3-turbo" . 3)
                                           ("large-v3" . 2)
                                           ("large-v2" . 2)
                                           ("large-v1" . 2)
                                           ("medium" . 5)
                                           ("small" . 7)
                                           ("base" . 10)
                                           ("tiny" . 13))
  "Alist that associates  sets the max transcription threads.
The LM used may impose additional constrains (whispers default max is 4)."
  :type '(alist :key-type string :value-type integer)
  :group 'voicememo)

(defcustom voicememo-audio-heading-hint "audio"
  "Voicememos are linked from the heading of the org entry.
This ist the name of the link."
  :type 'string
  :group 'voicememo)

(defcustom voicememo-max-heading-len 33
  "Max length of a heading of the org entry before an ellispsis is inserted.
Headings are prefixed with `voicememo-audio-heading-hint' remaining chars
are taking from the transcription of the voicememo."
  :type 'integer
  :group 'voicememo)

(defcustom voicememo-queue-timer 3
  "Try to que a new transcription proc every x seconds.
Defaults to 3."
  :type 'integer
  :group 'voicememo)

(defcustom voicememo-use-animation-p t
  "If set to t will pulse an org subtree associated with a transcription.
Defaults to t."
  :type 'boolean
  :group 'voicememo)

(defvar voicememo--curr-procs '()
  "Process objects of currently running transcription (whisper) processes.")

(defvar voicememo--entry-ids-alist '()
  "Associates an audiofile with the org-id of its org entry.")

(defun voicememo--full-filepath (audiofile)
  "Return full path of basename AUDIOFILE."
  (expand-file-name audiofile voicememo-src-dir))

(defun voicememo--str-from-file (file-path)
  "Return content of FILE-PATH as string."
  (with-temp-buffer
    (insert-file-contents file-path)
    (buffer-string)))

(defun voicememo--read-lastseen ()
  "Return last-seen timestamp."
  (let ((lastseen (voicememo--str-from-file voicememo-lastseen-file)))
    (if (string-empty-p lastseen)
        "0"
      lastseen)))

(defun voicememo--write-lastseen (str)
  "Write STR to lastseen-file."
  (write-region str nil voicememo-lastseen-file))

(defun voicememo--strip-non-numeric (str)
  "Strip everything from STR but the numbers."
  (replace-regexp-in-string "[^0-9]" "" str))

(defun voicememo--file-timestamp (audiofile)
  "Get timestamp from filename AUDIOFILE."
  (string-to-number
   (voicememo--strip-non-numeric
    (file-name-base audiofile))))

(defun voicememo--read-lastseen-timestamp ()
  "Convert content of .lastseen to timestamp."
  (string-to-number
   (voicememo--strip-non-numeric
    (voicememo--read-lastseen))))

(defun voicememo--recent-vms ()
  "Get all voicememos more recent that the timestamp in .lastseen."
  (seq-filter (lambda (audiofile)
                (> (voicememo--file-timestamp audiofile)
                   (voicememo--read-lastseen-timestamp)))
              (directory-files voicememo-src-dir nil "^[^.]")))

(defsubst voicememo--rel-file-path (audiofile)
  "Relative path to AUDIOFILE."
  (format "%s/%s" (file-name-base voicememo-src-dir) audiofile))

(defun voicememo--rel-file-link (audiofile)
  "Return string that corresponds to an org-link to AUDIOFILE."
  (format "[[file:%s][%s]]"
          (voicememo--rel-file-path audiofile)
          voicememo-audio-heading-hint))

(defun voicememo--inbox-heading (audiofile)
  "String that is inserted to create an org-heading for AUDIOFILE."
  (format "* %s %s"
          (voicememo--rel-file-link audiofile)
          "queued for transcription"))

(defun voicememo--whisper-buffername-stdout (audiofile)
  "Name of stdout buffer for AUDIOFILEs transcription process."
  (concat "*voicememo-whisper-"
          (file-name-base audiofile)
          "-stdout*"))

(defun voicememo--whisper-buffername-stderr (audiofile)
  "Name of stderr buffer for AUDIOFILEs transcription process."
  (concat "*voicememo-whisper-"
          (file-name-base audiofile)
          "-stderr*"))

(defun voicememo--queue-whisper-proc (audiofile whisper-fn)
  "Run WHISPER-FN on AUDIOFILE in new proc.
Check if the number of existing processes is less than max-procs first."
  (let ((max-procs (alist-get whisper-model voicememo-max-whisper-threads
                              1 nil #'string-equal)))
    (if (>= (length voicememo--curr-procs) max-procs)
        (run-with-timer voicememo-queue-timer nil
                        (lambda () (voicememo--queue-whisper-proc audiofile whisper-fn)))
      (message "start voicememo proc %d/%d" (+ 1 (length voicememo--curr-procs)) max-procs)
      (voicememo--set-properties
       audiofile `((,voicememo-status-prop . "transcribing")))
      (funcall whisper-fn audiofile))))

(defun voicememo--process-done-event-p (event)
  "Return t if EVENT is the last signal of a process."
  (or (string-equal "finished\n" event)
      (string-equal "terminated\n" event)
      (string-match "exited abnormally with code \\([0-9]\\).*" event)))

(defun voicememo--filename->orgid (audiofile)
  "Return org-id of AUDIOFILE heading."
  (alist-get audiofile voicememo--entry-ids-alist
             nil nil #'string-equal))

;;; The following functions follow a context pattern
;;; where the context is always the first argument.
;;; This allows deduplication of the code, so the
;;; discovery of org entries can be written only once.
;;; The transforming functions therefore can simply be
;;; written as if the point was resting on them.

(defun voicememo--update-current-heading-title (context new-title)
  "Update the text of the current heading to NEW-TITLE.
If CONTEXT is non-nil it should be an alist including the heading-id.
CONTEXT is injected by `voicememo--with-point-at-heading-...' wrappers."
  (let ((current-level (org-element-property :level (org-element-at-point)))
        (heading-id (alist-get 'heading-id context "<unknown>")))
    (if current-level
        (progn
          (delete-region (point) (eol))
          (insert (format "%s %s" (make-string current-level ?*) new-title))
          (message "Title of heading %s updated." heading-id))
      (message "Error: Unable to determine current level of heading %s." heading-id)))
  (save-buffer))

(defun voicememo--with-point-at-heading-of-id (context heading-id fn &rest args)
  "Execute FN(ARGS) and point at heading with HEADING-ID.
CONTEXT will be passed as firs arg to FN.
It is an alist containing HEADING-ID and the corresponding audiofile."
  (let ((el-marker (org-id-find heading-id 'marker)))
    (if (not el-marker)
        (message "Heading with ID %s not found." heading-id)
      (save-excursion
        (with-current-buffer (marker-buffer el-marker)
          (goto-char (marker-position el-marker))
          (apply fn context args))))))

(defun voicememo--with-point-at-heading-of-audiofile (context audiofile fn &rest args)
  "Execute FN(ARGS) and point at corresponding to AUDIOFILE.
CONTEXT will be passed as first arg to FN.
It is an alist containing heading-id and the corresponding AUDIOFILE."
  (let* ((id (voicememo--filename->orgid audiofile))
         (ctx (cons `(heading-id . ,id) context)))
    (apply #'voicememo--with-point-at-heading-of-id
           ctx id fn args)))

(defun voicememo--update-heading-title (audiofile new-title)
  "Find Org mode heading of AUDIOFILE and update its title to NEW-TITLE."
  (let ((context `(audiofile ,audiofile)))
    (voicememo--with-point-at-heading-of-audiofile
     context audiofile
     #'voicememo--update-current-heading-title
     new-title)))

(defun voicememo--set-content-of-current-heading (_ content)
  "Update the content of the current heading to CONTENT.
If CONTEXT is non-nil it should be an alist including the heading-id.
CONTEXT is injected by `voicememo--with-point-at-heading-...' wrappers."
  (org-end-of-meta-data)
  ;; (delete-region (point) (org-end-of-subtree t t))
  (insert content)
  (save-buffer))

(defun voicememo--set-heading-content (audiofile content)
  "Find Org mode heading of AUDIOFILE and set its content to CONTENT."
  (let ((context `(audiofile ,audiofile)))
    (voicememo--with-point-at-heading-of-audiofile
     context audiofile
     #'voicememo--set-content-of-current-heading
     content)))

(defun voicememo--insert-transcription (audiofile transcript)
  "Insert TRANSCRIPT into heading of AUDIOFILE.
Wraps it in a quote-block and appends extra '\n'.
It also prepends the result of `voicememo--org-timestamp',
this way entries resemble `org-gtd'."
  (voicememo--set-heading-content
   audiofile (concat (voicememo--org-timestamp audiofile)
                     "\n#+begin_quote\n"
                     transcript
                     "\n#+end_quote\n\n")))

(defun voicememo--truncate-string-with-ellipsis (str max-length)
  "Truncate STR to MAX-LENGTH characters and append ellipsis if needed."
  (if (<= (length str) max-length)
      str
    (concat (substring str 0 (- max-length 2)) "…")))

(defun voicememo--updated-heading-text (audiofile transcription)
  "After finishing it, update heading for AUDIOFILE.
New heading is a truncated version of TRANSCRIPTION."
  (concat
   (voicememo--rel-file-link audiofile)
   ": /" (voicememo--truncate-string-with-ellipsis
          (replace-regexp-in-string "\n" " " transcription)
          voicememo-max-heading-len) "/"))

(defun voicememo--trim-buffer ()
  "Remove whitespace, including newlines, at the beginning and end of the buffer."
  (save-excursion
    (goto-char (point-min))
    (skip-chars-forward " \n")
    (when (> (point) (point-min))
      (delete-region (point-min) (point)))
    (goto-char (point-max))
    (skip-chars-backward " \n")
    (when (> (point-max) (point))
      (delete-region (point) (point-max)))))

(defun voicememo--lisp-time (audiofile)
  "Extract timestamp from AUDIOFILE and return it as Lisp time-obj."
  (pcase-let
      ((`(,YEAR ,MON ,DAY ,HOUR ,MIN ,SEC) (string-split
                                            (file-name-base audiofile)
                                            "[- \\.]"))
       (DST -1)                         ;`encode-time' can guess daylight saving t
       (TZ nil))                        ;`encode-time' emacs-local timezone
    (encode-time
     (mapcar (lambda (el)
               (if (stringp el)
                   (string-to-number el)
                 el))
             (list SEC MIN HOUR DAY MON YEAR nil DST TZ)))))

(defun voicememo--org-timestamp (audiofile)
  "Return inactive timestamp with time extracted from AUDIOFILE."
  (format-time-string
   (concat "[" (cdr org-timestamp-formats) "]")
   (voicememo--lisp-time audiofile)))

(defun voicememo--set-properties* (heading-id properties)
  "For heading with HEADING-ID, insert PROPERTIES into the property drawer.
PROPERTIES should be an alist where each cell is a (KEY . VALUE) pair.
Example usage:
\\=(voicememo--set-properties \"8d7dbdb3-4f59-4fd1-a1e9-119fad986af9\"
\\='((\"KEY1\" . \"VALUE1\") (\"KEY2\" . \"VALUE2\")))."
  (if (or (not heading-id) (string-empty-p heading-id))
      (message "No org-id provided")
    (let ((el-marker (org-id-find heading-id 'marker)))
      (if (not el-marker)
          (message "Heading with ID %s not found." heading-id)
        (save-excursion
          (with-current-buffer (marker-buffer el-marker)
            (goto-char el-marker)
            (dolist (pair properties)
              (pcase pair
                (`(,key . ,value) (org-set-property key value))))
            (save-buffer)))))))

(defun voicememo--set-properties (audiofile properties)
  "For heading of AUDIOFILE insert into property drawer the alist PROPERTIES.
Keys of the alist are keys of the drawer, the same goes for values."
  (voicememo--set-properties* (voicememo--filename->orgid audiofile)
                              properties))

(defun voicememo--whisper-transcribe (audiofile)
  "Start audio transcribing process of AUDIOFILE in the background.
AUDIOFILE is expected to be a basename file in `voicememo-src-dir'."
  (let ((fpath (voicememo--full-filepath audiofile))
        (stdout-buffername (voicememo--whisper-buffername-stdout audiofile))
        (stderr-buffername (voicememo--whisper-buffername-stderr audiofile)))
    (if (not (file-exists-p fpath))
        (error "File can not be accessed: %s" fpath)
      (message (concat "[-] Queue audio for transcription: " audiofile))
      (voicememo--queue-whisper-proc
       audiofile
       (lambda (audio) "Whisper-fn running on AUDIO file"
         (message "[START] transcribing %s" audio)
         (let ((proc (make-process
                      :name (concat "whisper-transcribing-" audio)
                      ;; TODO dependency on whisper pkg
                      :command (whisper-command fpath)
                      :connection-type nil
                      :buffer (get-buffer-create stdout-buffername)
                      :stderr (get-buffer-create stderr-buffername)
                      :coding 'utf-8
                      :sentinel
                      (lambda (proc event)
                        (unwind-protect
                            (when-let* ((stdout-buffer (get-buffer stdout-buffername))
                                        (finished (and (buffer-live-p stdout-buffer)
                                                       (string-equal "finished\n" event))))
                              (with-current-buffer stdout-buffer
                                (voicememo--trim-buffer)
                                (goto-char (point-min))
                                ;; TODO dependency on whisper pkg
                                (run-hooks 'whisper-post-process-hook)
                                (if (= (buffer-size) 0)
                                    (error "Whisper command produced no output")
                                  (let ((transcription (buffer-string)))
                                    (voicememo--insert-transcription
                                     audio transcription)
                                    (voicememo--update-heading-title
                                     audio
                                     (voicememo--updated-heading-text
                                      audio transcription))))
                                ;; TODO: The file is now updated after the proc is started
                                ;; not after it finished. If this works as intended this
                                ;; comment can be removed.
                                ;; (if (> (voicememo--file-timestamp audio)
                                ;;        (voicememo--read-lastseen-timestamp))
                                ;;     (voicememo--write-lastseen
                                ;;      (file-name-base audio)))
                                ))
                          (when (voicememo--process-done-event-p event)
                            (message "[STOP] transcribing %s (received %s)"
                                     audio event)
                            ;; Ensure process is dead to prevent `kill-buffer' prompt
                            (ignore-errors (kill-process proc))
                            (setq voicememo--curr-procs
                                  (delq proc voicememo--curr-procs))
                            (ignore-errors (kill-buffer stdout-buffername))
                            (voicememo--set-properties
                             audio `((,voicememo-status-prop . ,(string-trim event))))))))))
           (setq voicememo--curr-procs
                 (cons proc voicememo--curr-procs))
           (when voicememo-use-animation-p
             (voicememo--pulse-org-subtree-until-process audio proc))
           ;; Update the last-seen file to prevent subsequent invocations of `voicememo-scan'
           ;; to insert duplicates
           (if (> (voicememo--file-timestamp audio)
                  (voicememo--read-lastseen-timestamp))
               (voicememo--write-lastseen
                (file-name-base audio)))))))))

(defun voicememo--add-audiofile-to-alist (audiofile)
  "Add AUDIOFILE to `voicememo--entry-ids-alist` with a generated Org ID.
If AUDIOFILE already exists in the alist, signal an error and report the
associated Org ID and the position of the heading in the buffer.
Prompts the user to jump to the heading if it exists."
  (let ((existing-entry (assoc audiofile voicememo--entry-ids-alist)))
    (if existing-entry
        (let ((id (cdr existing-entry)))
          (if-let ((marker (org-id-find id 'marker))) ;; Find the marker for the org entry
              (if (y-or-n-p (format "Audiofile '%s' is already associated with Org ID '%s' at buffer position %d. Jump to it? "
                                    audiofile id (marker-position marker)))
                  (progn
                    (switch-to-buffer (marker-buffer marker))
                    (goto-char (marker-position marker)))
                (error "Audiofile '%s' is already associated with Org ID '%s'" audiofile id))
            (error "Audiofile '%s' is already associated with Org ID '%s', but the Org entry no longer exists"
                   audiofile id)))
      (let ((id (org-id-get-create)))
        (setq voicememo--entry-ids-alist
              (cons (cons audiofile id) voicememo--entry-ids-alist))))))

(defun voicememo--create-entry (audiofile)
  "Create an org entry in the inbox for AUDIOFILE."
  (let ((file-path (voicememo--full-filepath audiofile)))
    (if (not (file-exists-p file-path))
        (message "can't find file: %s" file-path)
      (with-current-buffer (get-file-buffer voicememo-target-file)
        (goto-char (point-max))
        (insert
         "\n" (voicememo--inbox-heading audiofile) "\n")
        (voicememo--add-audiofile-to-alist audiofile)))))

(defun voicememo-scan ()
  "Scan `voicememo-src-dir' for recent additions.
For each one add an entry to `voicememo-target-file'.
Voicememos are parsed locally with the whisper model from openai."
  (interactive)
  (if-let ((memos (voicememo--recent-vms)))
      (mapc (lambda (audiofile)
              (voicememo--create-entry audiofile)
              (voicememo--set-properties
               audiofile
               `((,voicememo-record-time-prop . ,(voicememo--org-timestamp
                                                  audiofile))
                 (,voicememo-file-prop . ,(voicememo--rel-file-path
                                           audiofile))
                 (,voicememo-status-prop . "pending")))
              (voicememo--whisper-transcribe audiofile))
            memos)
    (message "No new Voicememos found.")))

(defun voicememo-delete-linked-audio-file ()
  "Delete the audio file linked in the AUDIOFILE prop of the current org-heading."
  (interactive)
  (when (org-at-heading-p)
    (save-excursion
      (let ((audio-file (org-entry-get nil voicememo-file-prop)))
        (when (and audio-file (file-exists-p audio-file))
          (delete-file audio-file)
          (message "Deleted audio file: %s" audio-file))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                                        ; Aesthetics

(defun voicememo--pulse-org-subtree-by-id (id)
  "Pulse the Org subtree associated with the given ID property."
  (let ((pos (org-find-property "ID" id)))
    (when pos
      (save-excursion
        (goto-char pos)
        (org-back-to-heading t)
        (let ((start (point))
              (end (org-end-of-subtree t t)))
          (pulse-momentary-highlight-region start end 'pulse-highlight-start-face))))))

(defun voicememo--pulse-org-subtree-until-process (audiofile process)
  "Continuously pulse the Org subtree of AUDIOFILE until PROCESS finishes."
  (let ((timer nil)
        (id (voicememo--filename->orgid audiofile)))
    (setq timer
          (run-with-timer
           0 1  ;; Start immediately, repeat every 1 second(s)
           (lambda ()
             (if (not (process-live-p process))
                 (cancel-timer timer) ;; Stop pulsing when the process is done
               (voicememo--pulse-org-subtree-by-id id)))))))

(defvar voicememo--link-rx (format "\\[\\[file:\\(.*?\\)\\]\\[%s\\]\\]" voicememo-audio-heading-hint))

(defun voicememo--get-linked-audio-file ()
  "Retrieve the file path of the audio file.
It returns the one linked in the current org-heading's content."
  (save-excursion
    (org-back-to-heading t)
    (let ((element (org-element-at-point)))
      (when (eq (org-element-type element) 'headline)
        (let ((content (org-element-property :title element)))
          (when (string-match voicememo--link-rx content)
            (match-string 1 content)))))))

(defun voicememo-transcribe-entry-at-point ()
  "Transcribe the audiofile of the entry at point.
Will update it's content and heading as if it were added by `voicememo-scan'.
It tries to retrieve the audiofile from `voicememo-file-prop'.
If that is not possible it falls back to parsing the headline link."
  (interactive)
  (let ((audiofile (file-name-nondirectory
                    (or (org-entry-get nil voicememo-file-prop)
                        (voicememo--get-linked-audio-file)))))
    (voicememo--add-audiofile-to-alist audiofile)
    (voicememo--whisper-transcribe audiofile)))

(defun voicememo-process-pending-transcriptions ()
  "Process all unfinished transcription entries.
These are all headings with a `voicememo-status-prop' property
that is not \"finished\"."
  (interactive)
  (if voicememo--curr-procs
      (message "Detected ongoing transcriptions. Wait until these finish.")
    (setq voicememo--entry-ids-alist nil)  ;; Otherwise aborted entries may register as duplicates
    (let ((scope (list voicememo-target-file))
          (match (format "%s={.+}" voicememo-status-prop)) ;; Include only entries with TRANSCRIPTION_STATUS
          (skip  (lambda () "Skip entries where TRANSCRIPTION_STATUS is \"finished\""
                   (when (string= (org-entry-get (point) voicememo-status-prop) "finished")
                     (save-excursion
                       (org-end-of-subtree t t))))))
      (org-map-entries #'voicememo-transcribe-entry-at-point
                       match scope skip))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                                        ; TOOLING (one-off functions)
                                        ; Code may be removed

(defun voicememo-clean-duplicates (&optional total-deleted)
  "Remove duplicate entries in `voicememo-target-file'.
The function runs recursively, TOTAL-DELETED is state from previous runs.
Duplicates are identified based on the property `voicememo-file-prop'.
Keeps only the first entry for each unique value of the property."
  (interactive)
  (unless (and (boundp 'voicememo-file-prop) voicememo-file-prop)
    (error "Variable `voicememo-file-prop` is not defined or is nil"))
  (let ((scope (list voicememo-target-file))
        (match (format "%s={.+}" voicememo-status-prop)))
    (let* ((seen-values (make-hash-table :test 'equal))
           (deleted-count 0)
           (delete-duplicate-fn
            (lambda ()
              (let ((prop-value (org-entry-get nil voicememo-file-prop)))
                (if (gethash prop-value seen-values)
                    (progn
                      (org-cut-subtree)
                      (cl-incf deleted-count))
                  (puthash prop-value t seen-values))))))
      (org-map-entries delete-duplicate-fn match scope)
      (setq total-deleted (+ (or total-deleted 0) deleted-count))
      (if (> deleted-count 0)
          (voicememo-clean-duplicates total-deleted) ; Recursively clean duplicates if any were removed
        (with-current-buffer (find-buffer-visiting voicememo-target-file)
          (save-buffer))
        (message "Removed %d duplicate entries based on property '%s'"
                 total-deleted voicememo-file-prop)))))

(defun voicememo-ensure-audiofile-property ()
  "Ensure that the AUDIOFILE property exists in the property drawer of the current heading.
If it does not exist but an audio link is present in the heading, add it."
  (interactive)
  (when (org-at-heading-p)
    (save-excursion
      (let ((existing (org-entry-get nil voicememo-file-prop)))
        (unless existing
          (let ((audio-file (voicememo--get-linked-audio-file)))
            (when audio-file
              (org-entry-put nil voicememo-file-prop audio-file)
              (message "Added %s property: %s" voicememo-file-prop audio-file))))))))

(defun voicememo-ensure-status-property ()
  "Ensure that the TRANSCRIPTION_STATUS property exists in the property drawer of the current heading.
If it does not exist but an audio link is present in the heading, add it."
  (when (org-at-heading-p)
    (save-excursion
      (let ((existing (org-entry-get nil voicememo-status-prop)))
        (unless existing
          (let ((audio-file (voicememo--get-linked-audio-file)))
            (when audio-file
              (org-entry-put nil voicememo-status-prop "finished")
              (message "Added %s property: %s" voicememo-status-prop "finished"))))))))

(defun voicememo-ensure-audiofile-property-buffer-wide ()
  (interactive)
  (org-map-entries
   #'voicememo-ensure-audiofile-property))

;; (let  ((keep (mapcar
;;               #'f-filename
;;               (-non-nil
;;                (org-map-entries
;;                 #'voicememo--get-linked-audio-file
;;                 t
;;                 '("~/Documents/notes/org/gtd/gtd.org"
;;                   "~/Documents/notes/org/gtd/inbox.org"))))))
;;   (let ((files (directory-files "~/Documents/notes/org/gtd/voice-inbox/" t)))
;;     (dolist (f files)
;;       (unless (or (member (f-filename f) keep)
;;                   (file-directory-p f)
;;                   (string= (f-filename f) ".last-seen"))
;;         (message "> %s" f)
;;         ;; (delete-file f)
;;         ))))

;; (org-map-entries
;;  #'voicememo-ensure-status-property
;;  t
;;  '("~/Documents/notes/org/gtd/gtd.org"
;;    "~/Documents/notes/org/gtd/inbox.org"))

;; (defun voicememo-set-transcription-status-prop (val)
;;   (when (org-at-heading-p)
;;     (save-excursion)
;;     (org-entry-put (point) voicememo-status-prop val)))

(provide 'voicememo)
;;; voicememo.el ends here
