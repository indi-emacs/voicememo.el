---
author: Indiana
title: Readme
---

- [Voicememo.el](#voicememo.el)
  - [Description](#description)
  - [Visuals](#visuals)
  - [Installation](#installation)
    - [Prerequisites](#prerequisites)
    - [Doom](#doom)
  - [Usage](#usage)
  - [Configuration](#configuration)
  - [Support](#support)
  - [Roadmap](#roadmap)
  - [Contributing](#contributing)
  - [Authors and acknowledgment](#authors-and-acknowledgment)
  - [License](#license)
  - [Project status](#project-status)

# Voicememo.el

## Description

Voicememo.el creates org entries from a directory with audio files. The
org entries are created in a target file, the intended purpose is the
usage with a gtd-like workflow, where you'd have one org file as your
inbox.

It attempts to transcribe voicememo using a configurable command but is
written for the [Whisper](https://github.com/openai/whisper) model.
Original audio will be accessible via link from the org entry and the
transcription will be inserted as its content.

This repo currently

## Visuals

## Installation

### Prerequisites

Currently this code still depends on
[whisper.el](https://github.com/natrys/whisper.el), even though none of
its functionality is used, only its configuration is reused. This will
probably change.

1.  Setup [whisper.el](https://github.com/natrys/whisper.el)

2.  Clone the repo to a local `/target/path`

``` shell
cd </target/path> &&
git clone --depth 1 https://gitlab.com/indi-emacs/voicememo.el
```

### Doom

In your `packages.el` include:

``` commonlisp
(package! voicememo
  :type 'local
  :recipe (:local-repo "</target/path>/voicememo"))
```

Set the paths to where you want to transcribe from and to. If you use
`org-gtd` for example:

``` commonlisp
(use-package! voicememo
  :custom
  (voicememo-src-dir (concat org-gtd-directory "/voicememos"))
  (voicememo-target-file (concat org-gtd-directory "/" org-gtd-inbox ".org")))
```

## Usage

If you want to sync voicememos from your phone you need to set up some
kind of sync yourself, like Nextcloud for example. The filenames of the
recordings are expected to be in the format `%Y_%m_%d_%H_%M_%S.mp4`
which is what the app [Simple Voice
Recorder](https://github.com/SimpleMobileTools/Simple-Voice-Recorder)
produces.

There is just one entrypoint `voicememo-scan`, call it from wherever,
via `M-x voicememo-scan` or add it to a hook variable to be run
automatically.

## Configuration

## Support

## Roadmap

## Contributing

## Authors and acknowledgment

## License

Licensed under GPLv3

## Project status

This is a one-shot script, not really meant to be shared. If you can get
something out of it great, but currently no promises that I'll work on
this ever again.
